$Id$

; API
api = 2

; Core
core = 7.x

; Drupal project.
projects[drupal] = 7.14

; We point to our own installation profile here.
; This profile is the one that we actually are going to use.
projects[bc_enterprise][type] = profile
projects[bc_enterprise][download][type] = git
projects[bc_enterprise][download][url] = git@git.drupal.org:sandbox/pontusnilsson/1633974.git
projects[bc_enterprise][download][branch] = master

; Patch for core that fixes translation wtfs for entities.
projects[drupal][patch][] = http://drupal.org/files/entity_language-1495648-16.patch
